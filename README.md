**NOTE:** This is an exploratory treatise and the reader should take it for
what it's. If you've a suggestion/correction/addition, please submit a PR.

# A bit of functional programming in Raku

## Convention

When considered informative and to provide context for a paragraph discussing
some code snippet, the evaluation of a statement/expression is displayed
with `#=>`. The output from one of the I/O functions is indicated by
by `# OUTPUT`.

## Table of contents

* [What is functional programming?](#what-is-functional-programming)

## What is functional programming?

## Some functional features

### First-class functions

In Raku, functions can be passed around as values, just
like any other object. This makes functions and other code object *first-class
citizens*

```perl
sub sum( \a, \b ) {
    a + b
}

&sum.^name;
#=> Sub

&sum.signature
#=> (\a, \b)

&sum.arity
#=> 2
```
 In this example, we've created the object `sum` of type [`Sub`]().
 We can instropect the object (i.e., `&sum.^name`) and the object possess
 numerous methods that we can use to probe into the object. For example,
 invoking the `signature` method on a `Sub` object returns the subroutine's
 signature.

### Higher-order functions

Higher-order functions are functions that can accept another function as an
argument or return a function as a value. These functions allows us to write
expressive and succint programs. Furthermore, they can be used to write highly
composite functions by using simpler functions as building blocks.

Consider the Raku [`max`]() subroutine. By default, it returns the numerically
largest element from a list-like object. In addition to this, we can provide
as an argument and modify how `max` behaves.

Take the following array:

```perl6
my @team-bucciarati = 
  %(name => 'Bruno Bucciarati', stand => 'Sticky Fingers', age => 20),
  %(name => 'Leone Abbacchio',  stand => 'Moody Blues', age => 21),
  %(name => 'Pannacotta Fugo',  stand => 'Purple Haze', age => 16),
  %(name => 'Narancia Ghirga',  stand => 'Aerosmith', age => 17),
  %(name => 'Guido Mista',      stand => 'Sex Pistols', age => 18),
  %(name => 'Giorno Giovanna',  stand => 'Gold Experience', age => 15),
;
```

Applying the `max` function is done as follows:

```raku
max @team-bucciarati;
```

```
[Out]: {age => 21, name => Leone Abbacchio, stand => Moody Blues}
```

Since `max` is a higher-order function, we can provide another function as
an argument which will filter out each value and whose return value is
compared instead of the original. This function argument must be supplied as the
named argument `by`:

```raku
max @team, by => { $_<name>.chars };

[Out]: {age => 20, name => Bruno Bucciarati, stand => Sticky Fingers}
```

# Backyard

* Curried functions

